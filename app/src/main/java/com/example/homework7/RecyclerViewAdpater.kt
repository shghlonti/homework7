package com.example.homework7

import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.homework7.databinding.RecycletViewTemplBinding
import kotlinx.android.synthetic.main.recyclet_view_templ.view.*

class RecyclerViewAdpater(private val items:ArrayList<Item>,activity: MainActivity): RecyclerView.Adapter<RecyclerViewAdpater.ViewHolder>(){


    inner class ViewHolder(val binding: View) : RecyclerView.ViewHolder(binding) {
        private lateinit var item:Item

        fun bind() {
            binding.desc.text = item.description
            binding.title.text = item.title
            d("Binding Log",item.title)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = RecycletViewTemplBinding.inflate(inflater)
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recyclet_view_templ,parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
        d("Item",items[position].title)
    }

//    {
//        val model:Item = items[position]
//        holder.itemView.title.setText(model.title)
//        holder.itemView.desc.setText(model.description)
////        holder.itemView.imageitem.setImageResource()
////        val url:String  = "https://i.ytimg.com/vi/MPV2METPeJU/maxresdefault.jpg"
////        Glide.with(holder.itemView).load(url).into(holder.itemView.imageitem);
//    }
}