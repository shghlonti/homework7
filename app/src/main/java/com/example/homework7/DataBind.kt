package com.example.homework7

import android.widget.ImageView
import androidx.databinding.BindingAdapter

object DataBind {


    @JvmStatic
    @BindingAdapter("setImageFromUrl")
    fun setImageFromUrl(imageView: ImageView, image: Int) {
        imageView.setImageResource(image);
    }

}