package com.example.homework7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework7.databinding.RecycletViewTemplBinding
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val items = ArrayList<Item>()

    val binding: RecycletViewTemplBinding= DataBindingUtil.setContentView(this,R.layout.recyclet_view_templ)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()

    }

    private fun init(){
        name_List.layoutManager = LinearLayoutManager(this)
        val adapter = RecyclerViewAdpater(items,this)
        name_List.adapter = adapter

        DataLoader.getRequest("",object : CustomCallback{
            override fun onSuccess(result: String) {

                var itemModel = Gson().fromJson(result,Item::class.java)

                val model = Item(itemModel.title,itemModel.description,itemModel.img)
                binding.setItemView(model)

                items.add(itemModel)

                adapter.notifyDataSetChanged()

                d("item cover url", "${itemModel.img }")

            }
        })

    }

}
